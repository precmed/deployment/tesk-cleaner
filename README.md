# tesk-cleaner

tesk-cleaner is used to clean up completed kubernetes pods created by Tesk jobs which are older than a threshold (default is 100 seconds) using kubectl commands.
To determine if the task has finished the status and completion time of `*-outputs-filer` job are checked.

To deploy the kubernetes cronjob issue

`kubectl apply -f https://gitlab.com/precmed/deployment/tesk-cleaner/raw/master/cleaner-cron.yaml`
